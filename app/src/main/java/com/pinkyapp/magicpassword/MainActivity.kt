package com.pinkyapp.magicpassword

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var guessGame = GuessNum()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val setting = getSharedPreferences("magicPassword", Context.MODE_PRIVATE)
        val times = setting.getInt("TIMES", 0)
        if (times > 0) {
            val secret = setting.getInt("SECRET", 0)
            guessGame.secret = secret
            guessGame.times = times
            guessGame.biggest = setting.getInt("BIGGEST", 100)
            guessGame.smallest = setting.getInt("SMALLEST", 1)
            //counter.setText(times.toString())
            drawTheView()
        }

        btn_guess.setOnClickListener {
            val result = guessGame.diff(guessNum.text.toString().toInt())
            if(result == 0){
                guessCount.setText(guessGame.times.toString())
                guessItMsg.visibility = View.VISIBLE
            }else{
                drawTheView()
            }
        }

        btn_replay.setOnClickListener {
            reset()
        }

        //SharedPreferences

        //guessGame
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!guessGame.isEnd) {
            //save data
            getSharedPreferences("magicPassword", Context.MODE_PRIVATE)
                .edit()
                .putInt("SECRET", guessGame.secret)
                .putInt("TIMES", guessGame.times)
                .putInt("BIGGEST", guessGame.biggest)
                .putInt("SMALLEST", guessGame.smallest)
                .apply()
        }
    }

    fun drawTheView(){
        biggestNum.setText(guessGame.biggest.toString())
        smallestNum.setText(guessGame.smallest.toString())
        guessCount.setText(guessGame.times.toString())

    }

    fun reset(){
        guessGame.reset()
        drawTheView()
        guessItMsg.visibility = View.GONE
        guessNum.setText("")
    }
}