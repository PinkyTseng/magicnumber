package com.pinkyapp.magicpassword

import android.util.Log
import java.util.*

class GuessNum {
    var smallest = 1
    var biggest = 100

    var isEnd = false
    var secret = 0
    var times = 0

    init {
        reset()
    }

    fun diff(theGuess: Int) : Int{
        val result = theGuess - secret
        times++
        isEnd = if (theGuess == secret) true else false

        if(result > 0){
            biggest = theGuess
        }else if(result < 0){
            smallest = theGuess
        }

        return result
    }



    fun reset(){
        smallest = 1
        biggest = 100
        //isEnd = false
        secret = Random().nextInt(100)+1
        times = 0

        isEnd = false

        Log.d("GuessNum", "secret: $secret")
    }




}